import { useEffect, useState } from "react";
import ProductList from "../components/productList";
import { useInFavorites } from "../hooks";
import NoProducts from "../components/noProducts";
import PropTypes from "prop-types";

export function Favorites(props) {
  const [productList, setProductList] = useState([]);
  const [dataLoaded, setDataLoaded] = useState(null);

  const favorites = useInFavorites(props);

  useEffect(() => {
    setDataLoaded(false);
    if (props.productList && favorites) {
      const productsInFavorites = props.productList.filter((product) =>
        favorites.includes(product.article)
      );
      setProductList(productsInFavorites);
      setDataLoaded(true);
    }
  }, [favorites]);

  return (
    <>
      <main className="main">
        <section className="container">
          {dataLoaded && favorites.length === 0 ? (
            <NoProducts target="favorites" />
          ) : null}
          {props.productList && (
            <ProductList
              productList={productList}
              onLocalStorageChanged={props.onLocalStorageChanged}
            />
          )}
        </section>
      </main>
    </>
  );
}

Favorites.propTypes = {
  productList: PropTypes.arrayOf(PropTypes.object),
  onLocalStorageChanged: PropTypes.func,
};
