import ProductList from "../components/productList";
import { useEffect, useState } from "react";
import { useInCart } from "../hooks";
import NoProducts from "../components/noProducts";
import PropTypes from "prop-types";

export function Cart(props) {
  const [productList, setProductList] = useState([]);
  const [dataLoaded, setDataLoaded] = useState(null);

  const cart = useInCart(props);

  useEffect(() => {
    setDataLoaded(false);
    if (props.productList && cart) {
      const productsInCart = props.productList.filter((product) =>
        cart.includes(product.article)
      );
      setProductList(productsInCart);
      setDataLoaded(true);
    }
  }, [cart]);

  return (
    <>
      <main className="main">
        <section className="container">
          {dataLoaded && cart.length === 0 ? (
            <NoProducts target="cart" />
          ) : null}
          {props.productList && (
            <ProductList
              productList={productList}
              onLocalStorageChanged={props.onLocalStorageChanged}
            />
          )}
        </section>
      </main>
    </>
  );
}

Cart.propTypes = {
  productList: PropTypes.arrayOf(PropTypes.object),
  onLocalStorageChanged: PropTypes.func,
};
