import ProductList from "../components/productList";
import PropTypes from "prop-types";

export function Home(props) {
  return (
    <>
      <main className="main">
        <section className="container">
          <h2 className="title">Choose your iPhone!)</h2>
          {props.productList && (
            <ProductList
              productList={props.productList}
              onLocalStorageChanged={props.onLocalStorageChanged}
            />
          )}
        </section>
      </main>
    </>
  );
}

Home.propTypes = {
  onLocalStorageChanged: PropTypes.func,
  productList: PropTypes.arrayOf(PropTypes.object),
};
