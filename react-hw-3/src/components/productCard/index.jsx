import styles from "./ProductCard.module.scss";
import Button from "../button";
import Modal from "../modal";
import {
  addToCartModalDesc,
  deleteFromCartModalDesc,
} from "../../mock/modalDescriptions";
import { useEffect, useState } from "react";
import StarIcon from "../starIcon";
import PropTypes from "prop-types";

export default function ProductCard(props) {
  const [isFavorite, setIsFavorite] = useState(false);
  const [inCart, setInCart] = useState(false);
  const [modalIsOpen, setModalIsOpen] = useState(false);

  useEffect(() => {
    setInCart(props.inCart);
    setIsFavorite(props.isFavorite);
  }, [props]);

  const setIsInFavorite = (article) => {
    isFavorite
      ? props.deleteFromFavorites(article)
      : props.addToFavorites(article);

    setIsFavorite((prevState) => !prevState);
    props.onLocalStorageChanged();
  };
  const setIsInCart = () => {
    inCart
      ? props.deleteFromCart(props.article)
      : props.addToCart(props.article);

    setInCart((prevSate) => !prevSate);

    showModal();
    props.onLocalStorageChanged();
  };

  const showModal = () => {
    setModalIsOpen((prevState) => !prevState);
  };

  const createModal = (modalData, actionHandlers = []) => {
    const {
      header: modalHeader,
      text: modalText,
      actions: modalActions,
      closeButton: modalCloseBtn,
    } = modalData;
    const actions = modalActions.map((action) => (
      <Button
        backgroundColor={action.backgroundColor}
        text={action.text}
        key={action.key}
        onClick={() => actionHandlers[action.key]}
      />
    ));
    return (
      <Modal
        header={modalHeader}
        text={modalText}
        actions={actions}
        closeButton={modalCloseBtn}
        closeButtonHandler={() => showModal}
      />
    );
  };

  return (
    <>
      <div className={styles.cardItem}>
        <img className={styles.img} src={props.imgURL} alt="photo" />
        <div className={styles.header}>
          <h2 className={styles.title}>{props.name}</h2>
          <StarIcon
            onClick={setIsInFavorite}
            article={props.article}
            fill={isFavorite}
            className={styles.favoriteIcon}
          />
        </div>
        <div className={styles.description}>
          <div className={styles.color}>
            <span className={styles.label}>Color:</span>
            <span className={styles.value}>{props.color}</span>
          </div>
          <div className={styles.article}>
            <span className={styles.label}>Article:</span>
            <span className={styles.value}>{props.article}</span>
          </div>
        </div>
        <div className={styles.priceWrap}>
          <p className={styles.price}>{props.price}$</p>
          {inCart ? (
            <Button
              backgroundColor="#0A84FF"
              text="Delete from cart"
              onClick={() => showModal}
            />
          ) : (
            <Button
              backgroundColor="#0A84FF"
              text="Add to cart"
              onClick={() => showModal}
            />
          )}
        </div>
      </div>

      {modalIsOpen &&
        (inCart
          ? createModal(deleteFromCartModalDesc, [showModal, setIsInCart])
          : createModal(addToCartModalDesc, [showModal, setIsInCart]))}
    </>
  );
}

ProductCard.propTypes = {
  name: PropTypes.string,
  price: PropTypes.number,
  imgURL: PropTypes.string,
  article: PropTypes.number,
  color: PropTypes.string,
  isFavorite: PropTypes.bool,
  inCart: PropTypes.bool,
  addToFavorites: PropTypes.func,
  deleteFromFavorites: PropTypes.func,
  addToCart: PropTypes.func,
  deleteFromCart: PropTypes.func,
  onLocalStorageChanged: PropTypes.func,
};

ProductCard.defaultProps = {
  isFavorite: false,
  inCart: false,
};
