import styles from "./ProductList.module.scss";
import ProductCard from "../productCard";
import { useInCart, useInFavorites } from "../../hooks";
import PropTypes from "prop-types";
export default function ProductList(props) {
  const cart = useInCart(props);
  const favorites = useInFavorites(props);

  const addToFavorites = (article) => {
    if (!favorites.includes(article)) {
      const newFavorites = [...favorites, article];
      localStorage.setItem("productFavorites", JSON.stringify(newFavorites));
    }
  };

  const deleteFromFavorites = (article) => {
    const newFavorites = favorites.filter((product) => product !== article);
    localStorage.setItem("productFavorites", JSON.stringify(newFavorites));
  };
  const addToCart = (article) => {
    if (!cart.includes(article)) {
      const newCart = [...cart, article];
      localStorage.setItem("productInCart", JSON.stringify(newCart));
    }
  };

  const deleteFromCart = (article) => {
    const newCart = cart.filter((product) => product !== article);
    localStorage.setItem("productInCart", JSON.stringify(newCart));
  };

  return (
    <ul className={styles.productList}>
      {props.productList.map((product) => (
        <ProductCard
          key={product.article}
          {...product}
          isFavorite={favorites.includes(product.article)}
          inCart={cart.includes(product.article)}
          addToFavorites={addToFavorites}
          deleteFromFavorites={deleteFromFavorites}
          addToCart={addToCart}
          deleteFromCart={deleteFromCart}
          onLocalStorageChanged={props.onLocalStorageChanged}
        />
      ))}
    </ul>
  );
}

ProductList.propTypes = {
  onLocalStorageChanged: PropTypes.func,
  productList: PropTypes.arrayOf(PropTypes.object),
};
