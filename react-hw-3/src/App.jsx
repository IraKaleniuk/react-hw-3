import "./App.css";
import styles from "./App.scss";
import { Cart, Favorites, Home } from "./pages";
import { Route, Routes } from "react-router-dom";
import Header from "./components/header";
import { useState } from "react";
import { useFetch, useInCart, useInFavorites } from "./hooks";

function App() {
  const productList = useFetch("./products.json");
  const [localStorage, setLocalStorage] = useState(false);

  const cart = useInCart(localStorage);
  const favorites = useInFavorites(localStorage);

  const onLocalStorageChanged = () => {
    setLocalStorage((prevState) => !prevState);
  };
  return (
    <>
      <Header
        productsInFavorites={favorites.length}
        productsInCart={cart.length}
      />
      <Routes>
        <Route
          path="/"
          element={
            <Home
              onLocalStorageChanged={onLocalStorageChanged}
              productList={productList.data}
            />
          }
        />
        <Route
          path="/cart"
          element={
            <Cart
              productList={productList.data}
              onLocalStorageChanged={onLocalStorageChanged}
            />
          }
        />
        <Route
          path="/favorites"
          element={
            <Favorites
              productList={productList.data}
              onLocalStorageChanged={onLocalStorageChanged}
            />
          }
        />
      </Routes>
    </>
  );
}

export default App;
