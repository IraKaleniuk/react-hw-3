import { useEffect, useState } from "react";

export function useInFavorites(props) {
  const [favorites, setFavorites] = useState([]);

  useEffect(() => {
    const favorites = localStorage.getItem("productFavorites")
      ? JSON.parse(localStorage.getItem("productFavorites"))
      : [];
    setFavorites(favorites);
  }, [props]);

  return favorites;
}
