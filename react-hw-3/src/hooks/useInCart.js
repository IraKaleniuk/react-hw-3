import { useEffect, useState } from "react";

export function useInCart(props) {
  const [cart, setCart] = useState([]);

  useEffect(() => {
    const cart = localStorage.getItem("productInCart")
      ? JSON.parse(localStorage.getItem("productInCart"))
      : [];
    setCart(cart);
  }, [props]);

  return cart;
}
